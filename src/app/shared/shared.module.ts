import { StarModule } from './component/star/start.module';
import { AppPipeModule } from './pipe/app-pipe.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    AppPipeModule,
    StarModule
  ]
})
export class SharedModule { }
