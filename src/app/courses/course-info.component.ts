import { CourseService } from './course.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Course } from './course';

@Component({
  templateUrl: './course-info.component.html'
})
export class CourseInfoComponent implements OnInit {

  course: Course;

  constructor(
    private activatedRoute: ActivatedRoute,
    private courseService: CourseService,
    private router: Router
    ) {
  }

  ngOnInit(): void {
    const courseId = +this.activatedRoute.snapshot.paramMap.get('id');
    this.courseService.retrieveById(courseId)
      .subscribe({
        next: course => this.course = course,
        error: err => console.log(err)
      });
  }

  save():void {
    this.courseService.save(this.course)
      .subscribe({
        next: course => {
          console.log('Course saved with success');
          this.router.navigateByUrl('/');
        },
        error: err => console.log(err)
      })
  }
}
